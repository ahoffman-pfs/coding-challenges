# Filtering Products

Transform the JSON-like object in `data.js` into a human-readable format. Show only discounted items costing under twenty dollars. Please write your JS using no libraries/frameworks. No additional CSS is needed. Try to be declarative, not imperative.

![Results](results.png)

## Bonus

Using JavaScript's `Date` object, expand the result list to include products created no earlier than June of 2017.
