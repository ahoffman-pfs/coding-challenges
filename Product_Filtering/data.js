const someJSON = {
  "some": {
    "place": {
      "pretty": {
        "nested": {
          "items": [
            {
              "id": "334343",
              "name": "Product 1",
              "price": "10.00",
              "hasDiscount": true,
              "meta": {
                "createdAt": "2018-05-11"
              }
            },
            {
              "id": "99993",
              "name": "Product 2",
              "price": "14.00",
              "hasDiscount": false,
              "meta": {
                "createdAt": "2017-09-15"
              }
            },
            {
              "id": "23432",
              "name": "Product 3",
              "price": "22.00",
              "hasDiscount": true,
              "meta": {
                "createdAt": "2013-03-15"
              }
            },
            {
              "id": "88921",
              "name": "Product 4",
              "price": "15.00",
              "hasDiscount": true,
              "meta": {
                "createdAt": "2016-10-02"
              }
            },
            {
              "id": "23432",
              "name": "Product 5",
              "price": "19.99",
              "hasDiscount": true,
              "meta": {
                "createdAt": "2016-01-15"
              }
            },
          ]
        }
      }
    }
  }
};