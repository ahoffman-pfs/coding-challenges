# Chess Board

For this challenge, there are two parts.

## Part 1

Using only `CSS`, create an 8x8 black-and-white chess board. There are different ways to accomplish this, so use the best way you know.

### Guidelines

* The chess board should be 420px by 420px
* Don't let browser support affect your solution
* Bonus points for efficiency and modern techniques

When you're done, your board should look something like this:

![Board](chess_board.png)

## Part 2

Now that you've created the board, place a piece on the board and move it. The image asset `knight.png` exists within the current directory.

It should be initially placed in the following square:

![Chess board with piece](chess_board_with_piece.jpeg)

And moved here:

![Chess board with moved piece](chess_board_with_piece_moved.jpeg)

In chess, the knight piece can move in 'L' shapes, in any direction.

Your job is to animate the chess piece using the following path:

![Chess board solution path](chess_board_with_piece_path.jpeg)

### Guidelines

* Use `CSS` only to achieve this
* Constrain the oversized knight image to the dimensions of the square
* The animation should only run once
* Bonus points for elegance and modern techniques
