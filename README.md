# PFS Coding Challenges

Please read the following instructions before beginning.

## Clone the repository

To pull down the code:

```
$ git clone https://ahoffman-pfs@bitbucket.org/ahoffman-pfs/coding-challenges.git
$ cd <cloned-directory>
```

Create a branch, using the format `firstname_lastname`

* Replace `first` with your first name and `last` with your last name
* Use all lowercase letters
* Omit any apostrophes

For example, if your name is Dan O'Rourke:

```
$ git checkout -b dan_orourke
```

## The Exercises

`JavaScript` — [Product Filtering](https://bitbucket.org/ahoffman-pfs/coding-challenges/src/master/Product_Filtering/)

`CSS` — [Chess Board](https://bitbucket.org/ahoffman-pfs/coding-challenges/src/master/Chess_Board/)

## Submission Guidelines

Feel free to make as many commits as you'd like to your branch. In fact, multiple commits can be a nice way to show your work along the way to a solution. However, only push your code upstream when you believe it is ready for submission. Ideally, you will push code once per challenge section.

```
$ git push origin firstname_lastname
```
